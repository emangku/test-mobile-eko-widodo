package com.creativeit.testmobile_eko_widodo_081286541280.Api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FactoryService {
    public static WebServices create(){
        Retrofit retrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).baseUrl("http://erporate.com").build();
        return retrofit.create(WebServices.class);
    }
}
