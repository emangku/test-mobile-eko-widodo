package com.creativeit.testmobile_eko_widodo_081286541280.Api;

import com.creativeit.testmobile_eko_widodo_081286541280.models.ls_model.ListPariwisata;

import retrofit2.Call;
import retrofit2.http.GET;

public interface WebServices {
    @GET("/bootcamp/jsonBootcamp.php")
    Call<ListPariwisata> getPariwisata();
}
