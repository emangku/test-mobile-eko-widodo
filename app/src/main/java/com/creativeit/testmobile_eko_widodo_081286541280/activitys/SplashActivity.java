package com.creativeit.testmobile_eko_widodo_081286541280.activitys;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.animation.BounceInterpolator;
import android.widget.TextView;
import android.widget.Toast;

import com.creativeit.testmobile_eko_widodo_081286541280.R;

import java.util.ArrayList;
import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

public class SplashActivity extends AppCompatActivity {
    private static final long ANIMATION_DURATION = 5000L;
    private TextView textView;
    private static final int RC_LOCATION_CONTACTS_PERM = 124;
    Bundle bundle;
    private final static int REQUEST_CODE = 99;
    private final static int INTERVAL_DELAY = 3000;
    private static List<String> permissionList;
    boolean askOnceAgain = false;
    Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        init();
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode,permissions,grantResults,this);
    }
    @AfterPermissionGranted(RC_LOCATION_CONTACTS_PERM)
    private void methodRequiresTwoPermission(){
        String[] mParams = {
                Manifest.permission.INTERNET,Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION};
        if (EasyPermissions.hasPermissions(this, mParams)){
            startAnimation();
        }else {
            Toast.makeText(this,"Permission", Toast.LENGTH_SHORT).show();
        }
    }


    void init(){
        textView = findViewById(R.id.txtSplash);
        activity = this;
        checkPermissions();
        startAnimation();
    }

    @Override
    protected void onResume() {
        super.onResume();
        permissionList.clear();
        if (askOnceAgain){
            askOnceAgain = false;
            checkPermissions();
        }
    }

    private void  startAnimation(){
        ValueAnimator valueAnimator = ValueAnimator.ofFloat(0f,1f);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float value = (float) valueAnimator.getAnimatedValue();
                textView.setScaleX(value);
                textView.setScaleY(value);
            }
        });
        valueAnimator.setInterpolator(new BounceInterpolator());

        valueAnimator.setDuration(ANIMATION_DURATION);
        final Intent mIntent = new Intent(this, LoginActivity.class);
        valueAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                startActivity(mIntent);
                finish();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        valueAnimator.start();
    }

    private void checkPermissions(){
        int hasFineLocationPermission = ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION);
        int hasCurseLocationPermission = ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION);
        int hasInternetLocationPermission = ContextCompat.checkSelfPermission(activity, Manifest.permission.INTERNET);

        permissionList = new ArrayList<>();
        if (hasFineLocationPermission != PackageManager.PERMISSION_GRANTED){
            permissionList.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (hasCurseLocationPermission != PackageManager.PERMISSION_GRANTED){
            permissionList.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (hasInternetLocationPermission != PackageManager.PERMISSION_GRANTED){
            permissionList.add(Manifest.permission.INTERNET);
        }

        if (!permissionList.isEmpty()){
            ActivityCompat.requestPermissions(activity,permissionList.toArray(new String[permissionList.size()]),REQUEST_CODE);
        }
    }

}
