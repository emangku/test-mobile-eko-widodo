package com.creativeit.testmobile_eko_widodo_081286541280.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.creativeit.testmobile_eko_widodo_081286541280.R;
import com.creativeit.testmobile_eko_widodo_081286541280.m_interface.RecyclerItemClickListener;
import com.creativeit.testmobile_eko_widodo_081286541280.models.Pariwisata;

import java.util.List;

public class PariwisataAdapter extends RecyclerView.Adapter<PariwisataAdapter.ViewHolder> {
    private List<Pariwisata> list;
    private RecyclerItemClickListener recyclerItemClickListener;
    private Context context;
    public PariwisataAdapter(Context context, List<Pariwisata> list, RecyclerItemClickListener recyclerItemClickListener) {
        this.list = list;
        this.recyclerItemClickListener = recyclerItemClickListener;
        this.context = context;
    }


    @NonNull
    @Override
    public PariwisataAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_pariwisata, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PariwisataAdapter.ViewHolder holder, final int position) {
        Glide.with(context).load(list.get(position).getGambar_pariwisata()).into(holder.img);
        holder.nmPariwisata.setText(list.get(position).getNama_pariwisata());
        holder.APariwisata.setText(list.get(position).getAlamat_pariwisata());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recyclerItemClickListener.onItemClick(view.getContext(), list.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        if (list !=null){
            return list.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView nmPariwisata, APariwisata;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.viewImg);
            nmPariwisata = itemView.findViewById(R.id.viewTitle);
            APariwisata = itemView.findViewById(R.id.viewAlamat);
        }
    }
}
