package com.creativeit.testmobile_eko_widodo_081286541280.fragments;


import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.creativeit.testmobile_eko_widodo_081286541280.R;
import com.creativeit.testmobile_eko_widodo_081286541280.utils.utils;
import com.uncopt.android.widget.text.justify.JustifiedTextView;

import static android.graphics.text.LineBreaker.JUSTIFICATION_MODE_INTER_WORD;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailsFragment extends Fragment {
    private ImageView viewIMG;
    private TextView viewNmPariwiata;
    private TextView viewAlamat;
    private JustifiedTextView viewDetails;

    public DetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_details, container, false);
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
    }

    @SuppressLint("WrongConstant")
    private void init(View view){
        viewNmPariwiata = view.findViewById(R.id.viewNmPariwiata);
        viewAlamat = view.findViewById(R.id.viewAlamat);
        viewDetails = view.findViewById(R.id.viewDetails);
        viewIMG = view.findViewById(R.id.viewIMG);

        Glide.with(view.getContext()).load(utils.getDataSession(view.getContext(),"session_Gambar_Pariwisata")).into(viewIMG);
        viewNmPariwiata.setText(utils.getDataSession(view.getContext(),"session_NM_Pariwisata"));
        viewAlamat.setText(utils.getDataSession(view.getContext(),"session_Alamat_Pariwisata"));
        viewDetails.setText(Html.fromHtml("<p align='justify'>"+utils.getDataSession(view.getContext(),"session_Detail_Pariwisata")+"</p>"));
    }


}
