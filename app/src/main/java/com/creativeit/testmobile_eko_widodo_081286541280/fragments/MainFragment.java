package com.creativeit.testmobile_eko_widodo_081286541280.fragments;


import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.creativeit.testmobile_eko_widodo_081286541280.R;
import com.creativeit.testmobile_eko_widodo_081286541280.adapters.PariwisataAdapter;
import com.creativeit.testmobile_eko_widodo_081286541280.helpers.GPSTracking;
import com.creativeit.testmobile_eko_widodo_081286541280.impl.GetPariwisataIntractorImpl;
import com.creativeit.testmobile_eko_widodo_081286541280.m_interface.MainInterface;
import com.creativeit.testmobile_eko_widodo_081286541280.m_interface.MainPresenterImp;
import com.creativeit.testmobile_eko_widodo_081286541280.m_interface.RecyclerItemClickListener;
import com.creativeit.testmobile_eko_widodo_081286541280.models.Pariwisata;
import com.creativeit.testmobile_eko_widodo_081286541280.utils.utils;

import java.util.List;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment implements MainInterface.MainView, SwipeRefreshLayout.OnRefreshListener {
    private ProgressBar progressBar;
    private RecyclerView recyclerView;

    private MainInterface.presenter presenter;
    private SwipeRefreshLayout refresh;
    private double latitute;
    private double longitude;
    private GPSTracking gpsTracking;
    private NavController navController;

    public MainFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initProgressBar(view);
        showLoadingPage();
        init(view);
        presenter = new MainPresenterImp(this, new GetPariwisataIntractorImpl(view.getContext()));
        presenter.requestDataToServer();
    }

    private void init(View view){
        gpsTracking = new GPSTracking(view.getContext());
        if(gpsTracking.canGetLocation()){
            latitute = gpsTracking.getLatitude();
            longitude = gpsTracking.getLongitude();
        }
        navController = Navigation.findNavController(view);
        Toast.makeText(view.getContext(),"Loaction : "+latitute +","+longitude, Toast.LENGTH_LONG).show();
//        getActivity().setTitle("");
        recyclerView = view.findViewById(R.id.viewRcl);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        recyclerView.setLayoutManager(layoutManager);
        refresh = view.findViewById(R.id.refresh);
        refresh.setOnRefreshListener(this);
    }
    private void initProgressBar(View view){
        progressBar = new ProgressBar(view.getContext(), null, android.R.attr.progressBarStyleLarge);
        progressBar.setIndeterminate(true);

        RelativeLayout relativeLayout = new RelativeLayout(view.getContext());
        relativeLayout.setGravity(Gravity.CENTER);
        relativeLayout.addView(progressBar);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        progressBar.setVisibility(View.GONE);
        Objects.requireNonNull(getActivity()).addContentView(relativeLayout,params);
    }


    private RecyclerItemClickListener recyclerItemClickListener = new RecyclerItemClickListener() {
        @Override
        public void onItemClick(Context context, Pariwisata pariwisata) {
            utils.setDataSession(context,"session_NM_Pariwisata",pariwisata.getNama_pariwisata());
            utils.setDataSession(context,"session_Alamat_Pariwisata",pariwisata.getAlamat_pariwisata());
            utils.setDataSession(context,"session_Detail_Pariwisata",pariwisata.getDetail_pariwisata());
            utils.setDataSession(context,"session_Gambar_Pariwisata",pariwisata.getGambar_pariwisata());
            navController.navigate(R.id.toDetailFragment);

            Toast.makeText(context, "List title : "+pariwisata.getNama_pariwisata(), Toast.LENGTH_LONG).show();
        }
    };




    @Override
    public void showLoadingPage() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadingPage() {
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void setDataToView(Context context, List<Pariwisata> pariwisataArrayList) {
        PariwisataAdapter adapter = new PariwisataAdapter(context,pariwisataArrayList, recyclerItemClickListener);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onResponseFailure(Context context,Throwable t) {
        Toast.makeText(context,"omething went wrong...Error message: "+t.getMessage(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void onRefresh() {
        presenter.onRefresh();
        refresh.setEnabled(false);
    }
}
