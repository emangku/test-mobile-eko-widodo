package com.creativeit.testmobile_eko_widodo_081286541280.impl;


import android.content.Context;

import com.creativeit.testmobile_eko_widodo_081286541280.Api.FactoryService;
import com.creativeit.testmobile_eko_widodo_081286541280.Api.WebServices;
import com.creativeit.testmobile_eko_widodo_081286541280.m_interface.MainInterface;
import com.creativeit.testmobile_eko_widodo_081286541280.models.Pariwisata;
import com.creativeit.testmobile_eko_widodo_081286541280.models.ls_model.ListPariwisata;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetPariwisataIntractorImpl implements MainInterface.GetPariwisataIntractor {
    private Context context;
    private List<Pariwisata> list;
    public GetPariwisataIntractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void getPariwisataArrayList(final OnFinishedListener onFinishedListener) {
        WebServices services = FactoryService.create();
        Call<ListPariwisata> pariwisataCall = services.getPariwisata();
        System.out.println(pariwisataCall.request().url());
        pariwisataCall.enqueue(new Callback<ListPariwisata>() {
            @Override
            public void onResponse(Call<ListPariwisata> call, Response<ListPariwisata> response) {
                list = new ArrayList<>();
                list = response.body().getData();
                System.out.println("LIST :"+list.toString());
                onFinishedListener.onFinished(context,list);
            }

            @Override
            public void onFailure(Call<ListPariwisata> call, Throwable t) {
                onFinishedListener.onFailure(context,t);
            }
        });
    }
}
