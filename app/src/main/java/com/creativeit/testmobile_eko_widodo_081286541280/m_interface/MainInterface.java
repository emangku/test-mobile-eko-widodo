package com.creativeit.testmobile_eko_widodo_081286541280.m_interface;

import android.content.Context;

import com.creativeit.testmobile_eko_widodo_081286541280.models.Pariwisata;

import java.util.List;

public interface MainInterface {
    interface presenter{
        void onDestroy();
        void onRefresh();
        void requestDataToServer();
    }

    interface MainView{
        void showLoadingPage();
        void hideLoadingPage();
        void setDataToView(Context context, List<Pariwisata> pariwisataArrayList);
        void onResponseFailure(Context context,Throwable t);
    }

    interface GetPariwisataIntractor{
        interface OnFinishedListener{
            void onFinished(Context context,List<Pariwisata> list);
            void onFailure(Context context,Throwable t);
        }

        void getPariwisataArrayList(OnFinishedListener onFinishedListener);
    }

}
