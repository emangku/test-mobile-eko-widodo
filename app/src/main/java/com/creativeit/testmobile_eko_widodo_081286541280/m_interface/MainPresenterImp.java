package com.creativeit.testmobile_eko_widodo_081286541280.m_interface;

import android.content.Context;

import com.creativeit.testmobile_eko_widodo_081286541280.models.Pariwisata;

import java.util.List;

public class MainPresenterImp implements MainInterface.presenter, MainInterface.GetPariwisataIntractor.OnFinishedListener {
    private MainInterface.MainView mainView;
    private MainInterface.GetPariwisataIntractor getPariwisataIntractor;

    public MainPresenterImp(MainInterface.MainView mainView, MainInterface.GetPariwisataIntractor getPariwisataIntractor) {
        this.mainView = mainView;
        this.getPariwisataIntractor = getPariwisataIntractor;
    }

    @Override
    public void onDestroy() {
        mainView = null;
    }

    @Override
    public void onRefresh() {
        if (mainView !=null){
            mainView.showLoadingPage();
        }
        getPariwisataIntractor.getPariwisataArrayList(this);
    }

    @Override
    public void requestDataToServer() {
        getPariwisataIntractor.getPariwisataArrayList(this);
    }

    @Override
    public void onFinished(Context context, List<Pariwisata> list) {
        if (mainView !=null){
            mainView.setDataToView(context,list);
            mainView.hideLoadingPage();
        }
    }

    @Override
    public void onFailure(Context context,Throwable t) {
        if (mainView != null){
            mainView.onResponseFailure(context,t);
            mainView.hideLoadingPage();
        }
    }
}
