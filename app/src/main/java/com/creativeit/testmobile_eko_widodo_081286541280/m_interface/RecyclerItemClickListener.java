package com.creativeit.testmobile_eko_widodo_081286541280.m_interface;

import android.content.Context;

import com.creativeit.testmobile_eko_widodo_081286541280.models.Pariwisata;

public interface RecyclerItemClickListener {
    void onItemClick(Context context,Pariwisata pariwisata);
}
