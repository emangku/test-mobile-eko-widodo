package com.creativeit.testmobile_eko_widodo_081286541280.models.ls_model;

import com.creativeit.testmobile_eko_widodo_081286541280.models.Pariwisata;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ListPariwisata {
    @SerializedName("result") private String result;
    @SerializedName("data") private List<Pariwisata> data;

    public ListPariwisata() {
    }

    public ListPariwisata(String result, List<Pariwisata> data) {
        this.result = result;
        this.data = data;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public List<Pariwisata> getData() {
        return data;
    }

    public void setData(List<Pariwisata> data) {
        this.data = data;
    }
}
