package com.creativeit.testmobile_eko_widodo_081286541280.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;

public class utils {
    private static String SESSION_NAME = "acakacakajajugagapapa";
    private static SharedPreferences preferences;
    public static ProgressDialog showDialogShow(Context context, String msg){
        ProgressDialog dialog = new ProgressDialog(context);
        dialog.setMessage(msg);
        dialog.setCancelable(false);
        return dialog;
    }

    public static void setDataSession(Context context,String session, String values){
        preferences = context.getSharedPreferences(SESSION_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putString(session, values);
        edit.apply();
    }

    public static String getDataSession(Context context,String session){
        if(preferences == null){
            preferences = context.getSharedPreferences(SESSION_NAME, Context.MODE_PRIVATE);
        }
        return preferences.getString(session, null);
    }
}
